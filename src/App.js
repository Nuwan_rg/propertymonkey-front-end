import React from 'react';
import './App.css';
import LandingPageComponent from "./components/landingPage/landingpageComponent";
import LoginComponent from "./components/loginComponent/loginComponent";
import SearchBoxComponent from "./components/landingPage/searchBoxComponent/searchBoxComponent";

function App() {
  return (
    <div className="App">
        <LandingPageComponent />
    </div>
  );
}

export default App;
