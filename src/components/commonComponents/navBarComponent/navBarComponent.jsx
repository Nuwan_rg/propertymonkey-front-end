import React, {useState} from 'react';
import {Nav, Navbar, NavLink} from "react-bootstrap";
import "./navBarComponent.css";
import LoginComponent from "../../loginComponent/loginComponent";
import SignupComponent from "../../signupComponent/signupComponent";


function NavBarComponent() {
    const [isOpenLoginStatus, setIsLoginOpenStatus] = useState(
        {isOpenLogin: false}
    );
    const [isOpenSignupStatus, setIsSignupOpenStatus] = useState(
        {isOpenSignup: false}
    );

    function openLoginOrSignup(eventKey) {
        switch (eventKey) {
            case "login" : {
                setIsLoginOpenStatus({isOpenLogin: true});
                break;
            }
            case "signup" : {
                setIsSignupOpenStatus({isOpenSignup: true});
                break;
            }
            default: {
                break;
            }
        }
    }

    const closeLoginOrSignupDialog = function closeLoginOrSignup() {
        setIsLoginOpenStatus({isOpenLogin: false});
        setIsSignupOpenStatus({isOpenSignup: false});
    }

    return (
        <div>
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <img src="monkey-logo.png" className="logo" alt="Italian Trulli"/>
                <Navbar.Brand href="#home">Property Monkey</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link href="#buy">Buy</Nav.Link>
                        <Nav.Link href="#rent">Rent</Nav.Link>
                        <Nav.Link href="#apartments">Apartments</Nav.Link>
                        <Nav.Link href="#houses">Houses</Nav.Link>
                        <Nav.Link href="#apartments">New Projects</Nav.Link>
                        <Nav.Link href="#houses">Find Agents</Nav.Link>
                    </Nav>
                    <Nav onSelect={openLoginOrSignup}>
                        <NavLink eventKey="login">Login</NavLink>
                        <Nav.Link disabled={true}>or</Nav.Link>
                        <NavLink eventKey= "signup" >SignUp</NavLink>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
            <LoginComponent isOpen={isOpenLoginStatus.isOpenLogin} close={closeLoginOrSignupDialog}/>
            <SignupComponent isOpen={isOpenSignupStatus.isOpenSignup} close={closeLoginOrSignupDialog}/>
        </div>
    );

}

export default NavBarComponent;
